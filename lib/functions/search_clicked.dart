import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:weather_app/providers/base_providers.dart';

bool firstSearchClicked(WidgetRef ref) {
  final searchText = ref.watch(searchTextControllerProvider.notifier).state;

  debugPrint('func triggered');

  if (searchText.text.isEmpty) {
    //Trigger Empty Text Alert
    return false;
  } else {
    return true;
    // }
  }
}
