import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:open_weather_client/open_weather.dart';

import '../functions/search_clicked.dart';
import '../main.dart';
import '../providers/base_providers.dart';

class MySearchBar extends ConsumerWidget {
  const MySearchBar({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    submitFunc() async {
      ref.read(startSwapAnimationProvider.notifier).state = false;
      ref.refresh(currentDayIndexProvider.notifier).state;
      ref.read(weatherStatusProvider.notifier).state =
          ref.watch(startFirstPageAnimationProvider)
              ? WeatherStatus.loading
              : WeatherStatus.none;
      final searchText = ref.watch(searchTextControllerProvider).text;
      WeatherForecastData weatherData = await openWeather!
          .fiveDaysWeatherForecastByCityName(
              cityName: searchText, weatherUnits: WeatherUnits.METRIC)
          .catchError(
        (err) {
          if (err.toString().contains('city not found')) {
            ref.read(weatherStatusProvider.notifier).state =
                WeatherStatus.error;
            WeatherForecastData(forecastData: []);
          }
        },
      );
      ref.read(weatherStatusProvider.notifier).state = WeatherStatus.data;
      debugPrint(searchText);
      ref.read(fiveDaysWeatherListProvider.notifier).state = weatherData;
      // FocusManager.instance.primaryFocus?.unfocus();
      ref.read(startFirstPageAnimationProvider.notifier).state =
          firstSearchClicked(ref);
      // Future.delayed(
      //   const Duration(milliseconds: 2000),
      //   () {

      //   },
      // );
    }

    final startFirstAni = ref.watch(startFirstPageAnimationProvider);
    // return RawKeyboardListener(
    //   focusNode: FocusNode(),
    //   onKey: (value) {
    //     debugPrint('On KEy Event');
    //     debugPrint(value.logicalKey.keyId);
    //     // debugPrint(PhysicalKeyboardKey.enter.usbHidUsage);
    //     // if (value.logicalKey.keyId == LogicalKeyboardKey.enter.keyId) {
    //     //   debugPrint("enter Key");
    //     // }
    //     if (value.isKeyPressed(LogicalKeyboardKey.enter)) {
    //       debugPrint('Enter Logi Key');
    //       // FocusManager.instance.primaryFocus?.unfocus();
    //       Future.delayed(
    //         const Duration(milliseconds: 500),
    //         () {
    //           ref.read(startFirstPageAnimationProvider.notifier).state =
    //               firstSearchClicked(ref);
    //         },
    //       );
    //     }
    //   },
    return KeyboardListener(
      focusNode: FocusNode(),
      onKeyEvent: (value) async {
        if (value.logicalKey == LogicalKeyboardKey.enter) {
          debugPrint('Enter Logi Key');
          // FocusManager.instance.primaryFocus?.unfocus();
          Future.delayed(
            const Duration(milliseconds: 500),
            () {
              ref.read(startFirstPageAnimationProvider.notifier).state =
                  firstSearchClicked(ref);
            },
          );
        }
        if (value.physicalKey == PhysicalKeyboardKey.enter) {
          debugPrint('Enter Physi Key');
          submitFunc();
        }
      },
      child: SearchBar(
        onSubmitted: (value) async {
          submitFunc();
        },

        onChanged: (value) {},

        // focusNode: ref.read(focusNodeProvider),
        side: const MaterialStatePropertyAll(
          BorderSide(width: 1.5),
        ),
        elevation: const MaterialStatePropertyAll(0),
        hintText: 'City Name',
        trailing: [
          IconButton(
            onPressed: submitFunc,
            icon: const Icon(
              Icons.search,
            ),
          ),
        ],
        controller: ref.read(searchTextControllerProvider),
      )
          .animate(
            target: startFirstAni ? 1 : 0,
          )
          .then(
            delay: const Duration(milliseconds: 100),
            // duration: const Duration(milliseconds: 500),
          )
          .scaleX(begin: 1, end: 0.8)
          .scaleY(end: 0.8)
          // .slideY(end: -0.85)
          .slideX(end: 0.03)
          .align(begin: Alignment.center, end: Alignment.topRight),
      // ),
    );
  }
}
