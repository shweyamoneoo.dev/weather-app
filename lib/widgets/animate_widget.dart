import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';

class AnimateThisWidget extends StatelessWidget {
  const AnimateThisWidget(
      {super.key,
      required this.widget,
      this.startFirstAni = false,
      this.startSwapAni = false,
      required this.token,
      this.makeShimmer = true});
  final Widget widget;
  final bool startFirstAni;
  final bool startSwapAni;
  final int token;
  final bool makeShimmer;

  @override
  Widget build(BuildContext context) {
    // if (startSwapAni) {
    //   return widget.animate().flipH(begin: 1, duration: 250.ms);
    // }
    print('first');
    print(startFirstAni);
    print('swap');
    print(startSwapAni);
    final animateWidget = widget
        .animate(
          target: startFirstAni ? 1 : 0,
        )
        .flipH(begin: 1, duration: startSwapAni ? 250.ms : 0.ms)
        .then(
          delay: startSwapAni
              ? null
              : Duration(
                  milliseconds: (400 * (token == 5 ? token - 1 : token)) + 100),
        )
        // .align(end: Alignment.center)
        .fadeIn(
          begin: startSwapAni ? 1 : 0,
          duration: startSwapAni ? 0.ms : 500.ms,
        )
        .slide(
          begin: startSwapAni
              ? null
              : token == 4
                  ? const Offset(-1, 0)
                  : token == 5
                      ? const Offset(1, 0)
                      : const Offset(0, 1),
          end: const Offset(0, 0),
        )
        .animate(
          delay: startSwapAni
              ? null
              : token == 6
                  ? 2000.ms
                  : null,
          onComplete: (controller) => startSwapAni
              ? null
              : token == 6
                  ? controller.repeat()
                  : null,
        )
        .shimmer(
          color: !makeShimmer ? Colors.transparent : const Color(0x80FFFFFF),
          duration: startSwapAni
              ? null
              : token == 6
                  ? 300.ms
                  : null,
          delay: startSwapAni
              ? null
              : token == 6
                  ? 3000.ms
                  : null,
        )
        .scaleXY(
          duration: startSwapAni
              ? null
              : token == 6
                  ? 200.ms
                  : null,
          begin: startSwapAni
              ? 1
              : token == 6
                  ? 1
                  : 0,
          end: startSwapAni
              ? 1
              : token == 6
                  ? makeShimmer
                      ? 1.2
                      : 1.0
                  : null,
        )
        .then(
            delay: startSwapAni
                ? 0.ms
                : token == 6
                    ? 200.ms
                    : null)
        .scaleXY(
          end: startSwapAni
              ? 1
              : token == 6
                  ? makeShimmer
                      ? 1 / 1.2
                      : 1.0
                  : null,
        );
    return animateWidget;
    //   if (token == 7) {
    //     return widget.animate().flipH(begin: 1, duration: 250.ms);
    //   } else if (token == 6) {
    //     return widget
    //             .animate(
    //               target: startFirstAni ? 1 : 0,
    //               // onComplete: (controller) => controller.repeat(),
    //             )
    //             .then(
    //               delay: Duration(milliseconds: (400 * token) + 100),
    //             )
    //             .fadeIn(begin: 0)
    //             .animate(
    //               delay: const Duration(milliseconds: 2000),
    //               onComplete: (controller) => controller.repeat(),
    //             )
    //             .shimmer(
    //                 color: !makeShimmer
    //                     ? Colors.transparent
    //                     : const Color(0x80FFFFFF),
    //                 duration: const Duration(milliseconds: 300),
    //                 delay: const Duration(milliseconds: 3000))
    //             .scaleXY(
    //               duration: 200.ms,
    //               begin: 1,
    //               end: makeShimmer ? 1.2 : 1.0,
    //             )
    //             .then(delay: 200.ms)
    //             .scaleXY(
    //               end: makeShimmer ? 1 / 1.2 : 1.0,
    //             )
    //         // .shake(
    //         //   delay: const Duration(milliseconds: 2000),
    //         // );
    //         // .scaleX(begin: 1, end: 1.1)

    //         ;
    //   }
    //   Animate slideCondition = token == 4
    //       ? widget
    //           .animate(target: startFirstAni ? 1 : 0)
    //           .then(
    //             delay: Duration(milliseconds: (400 * token) + 100),
    //           )
    //           .align(end: Alignment.center)
    //           .fadeIn(begin: 0)
    //           .slideX(
    //             begin: -1,
    //             end: 0,
    //           )
    //       : token == 5
    //           ? widget
    //               .animate(target: startFirstAni ? 1 : 0)
    //               .then(
    //                 delay: Duration(milliseconds: (400 * (token - 1)) + 100),
    //               )
    //               .align(end: Alignment.center)
    //               .fadeIn(begin: 0)
    //               .slideX(
    //                 begin: 1,
    //                 end: 0,
    //               )
    //           : widget
    //               .animate(target: startFirstAni ? 1 : 0)
    //               .then(
    //                 delay: Duration(milliseconds: (400 * token) + 100),
    //               )
    //               .align(end: Alignment.center)
    //               .fadeIn(begin: 0)
    //               .slideY(
    //                 begin: 1,
    //                 end: 0,
    //               );
    //   return slideCondition;
  }
}
