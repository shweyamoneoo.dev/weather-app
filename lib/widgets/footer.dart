import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';

class MyFooter extends StatelessWidget {
  const MyFooter({
    super.key,
    required this.startFirstAni,
  });
  final bool startFirstAni;

  @override
  Widget build(BuildContext context) {
    return const Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Text.rich(
          TextSpan(
            text: 'Built By - ',
            children: [
              TextSpan(
                text: 'SYMO',
                style: TextStyle(color: Colors.yellowAccent),
              ),
            ],
            style: TextStyle(fontSize: 20),
          ),
        ),
        // Text.rich(
        //   TextSpan(
        //     text: 'Reference From - ',
        //     children: [
        //       TextSpan(
        //         text: 'Link',
        //         style: TextStyle(color: Colors.blueAccent),
        //       ),
        //     ],
        //   ),
        // ),
      ],
    ).animate(target: startFirstAni ? 1 : 0).fade(begin: 1, end: 0);
  }
}
