import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:open_weather_client/open_weather.dart';


final isResultEmptyProvider = StateProvider<bool>((ref) {
  return true;
});

final startFirstPageAnimationProvider = StateProvider<bool>((ref) {
  return false;
});

final startSwapAnimationProvider = StateProvider<bool>((ref) {
  return false;
});

final weatherStatusProvider = StateProvider<WeatherStatus>((ref) {
  return WeatherStatus.none;
});

final searchTextControllerProvider =
    StateProvider<TextEditingController>((ref) {
  return TextEditingController();
});

final currentDayIndexProvider = StateProvider<int>((ref) {
  return 0;
});

// final weatherFutureProvider =
//     FutureProvider.family((ref, String searchText) async {
//   return await openWeather!.fiveDaysWeatherForecastByCityName(
//       cityName: searchText, weatherUnits: WeatherUnits.METRIC);
// });

final fiveDaysWeatherListProvider = StateProvider<WeatherForecastData>((ref) {
  return WeatherForecastData(forecastData: []);
});

enum WeatherStatus { none, loading, data, error }
