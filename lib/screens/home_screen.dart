import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:weather_app/helper/date_time_helper.dart';
import 'package:weather_app/helper/string_extension.dart';

import '../providers/base_providers.dart';
import '../widgets/animate_widget.dart';
import '../widgets/footer.dart';
import '../widgets/search_bar.dart';

class HomeScreen extends HookConsumerWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final screenHeight = MediaQuery.sizeOf(context).height;
    final screenWidth = MediaQuery.sizeOf(context).width;
    final aniController = useAnimationController(
      duration: const Duration(milliseconds: 500),
    );
    final anima =
        Tween(begin: screenHeight * 0.15, end: 10).animate(aniController);
    final aniValue = useListenable(anima);

    final startFirstAni = ref.watch(startFirstPageAnimationProvider);
    if (startFirstAni) {
      aniController.forward();
    } else {
      aniController.reverse();
    }

    final fiveDaysWeatherList = ref.watch(fiveDaysWeatherListProvider);
    final startSwapAni = ref.watch(startSwapAnimationProvider);
    final index = ref.watch(currentDayIndexProvider);
    final weatherStatus = ref.watch(weatherStatusProvider);
    final cityName = ref.watch(searchTextControllerProvider);
    // print('focus${ref.watch(focusNodeProvider).hasFocus}');

    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Stack(
            fit: StackFit.expand,
            children: [
              Positioned(
                top: 0 + aniValue.value.toDouble(),
                left: 0,
                width: screenWidth,
                child: Image.asset(
                  'assets/app_icon/weather-app.png',
                  scale: 5,
                )
                    .animate(
                      target: startFirstAni ? 1 : 0,
                    )
                    .swap(
                      duration: const Duration(milliseconds: 250),
                      builder: (context, child) {
                        return IconButton(
                          icon: const Icon(Icons.restart_alt_outlined),
                          onPressed: () {
                            ref
                                .refresh(searchTextControllerProvider.notifier)
                                .state;
                            ref.refresh(weatherStatusProvider.notifier).state;
                            ref
                                .refresh(startSwapAnimationProvider.notifier)
                                .state;
                            ref
                                .refresh(
                                    startFirstPageAnimationProvider.notifier)
                                .state;
                            ref
                                .refresh(fiveDaysWeatherListProvider.notifier)
                                .state;
                            ref.refresh(currentDayIndexProvider.notifier).state;
                          },
                        );
                      },
                    )
                    .scaleXY(begin: 1, end: 0.7)
                    // .slideX(end: 0.2)
                    .align(begin: Alignment.center, end: Alignment.topLeft),
              ),
              const MySearchBar(),
              if (weatherStatus == WeatherStatus.loading)
                const SpinKitSpinningCircle(
                  color: Colors.grey,
                  size: 50.0,
                )
              else if (weatherStatus == WeatherStatus.error)
                const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    // Expanded(child: SizedBox()),
                    Icon(
                      Icons.error_outline,
                      size: kToolbarHeight / 1.5,
                      color: Colors.deepOrange,
                    ),
                    SizedBox(
                      height: kBottomNavigationBarHeight / 2.5,
                    ),
                    Text(
                      'This City Name Doesn\'t Exist!',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    // Expanded(child: SizedBox()),
                  ],
                ).animate(delay: 300.ms).fadeIn().shake()
              // .animate(delay: 500.ms)
              // .fadeIn()
              // .shake()
              // .scaleXY(end: 1.2, duration: 200.ms)
              // .then(delay: 100.ms)
              // .scaleXY(end: 1 / 1.2)
              else if (weatherStatus == WeatherStatus.data) ...[
                Positioned(
                  top: screenHeight / 10 * 1,
                  width: screenWidth,
                  child: AnimateThisWidget(
                    widget: Text(
                      cityName.text.isEmpty
                          ? 'City Name'
                          : cityName.text.capitalizeFirstLetters,
                      textAlign: TextAlign.center,
                      // : fiveDaysWeatherList.forecastData.first.name ?? '',
                      style: const TextStyle(
                        fontSize: 32,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    // startSwapAni: startSwapAni,
                    startFirstAni: startFirstAni,
                    token: 1,
                  ),
                ),
                Positioned(
                  top: screenHeight / 10 * 2,
                  width: screenWidth,
                  child: AnimateThisWidget(
                    key: Key('image$index'),
                    widget: fiveDaysWeatherList.forecastData.isEmpty
                        ? const SizedBox()
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.network(
                                'https://openweathermap.org/img/wn/${fiveDaysWeatherList.forecastData[index * 8].details.first.icon}@4x.png',
                                fit: BoxFit.cover,
                              ),
                            ],
                          ),
                    startSwapAni: startSwapAni,
                    startFirstAni: startFirstAni,
                    token: 2,
                  ),
                ),
                Positioned(
                  top: screenHeight / 10 * 5,
                  width: screenWidth - 10,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AnimateThisWidget(
                        widget: IconButton.filledTonal(
                          style: IconButton.styleFrom(
                              backgroundColor: Colors.grey.shade200),
                          onPressed: ref.watch(currentDayIndexProvider) == 0
                              ? null
                              : () {
                                  ref
                                      .read(startSwapAnimationProvider.notifier)
                                      .state = true;
                                  --ref
                                      .read(currentDayIndexProvider.notifier)
                                      .state;
                                },
                          icon: const Icon(Icons.chevron_left),
                        ),
                        startFirstAni: startFirstAni,
                        makeShimmer: ref.watch(currentDayIndexProvider) != 0,
                        token: 6,
                      ),
                      AnimateThisWidget(
                        key: Key('date$index'),
                        widget: Text(
                          DateTimeHelper.getDateTime(
                              fiveDaysWeatherList.forecastData[index * 8].date,
                              index),
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            fontSize: 22,
                          ),
                        ),
                        startSwapAni: startSwapAni,
                        startFirstAni: startFirstAni,
                        token: 3,
                      ),
                      AnimateThisWidget(
                        widget: IconButton.filledTonal(
                          style: IconButton.styleFrom(
                              backgroundColor: Colors.grey.shade200),
                          onPressed: ref.watch(currentDayIndexProvider) < 4
                              ? () {
                                  ref
                                      .read(startSwapAnimationProvider.notifier)
                                      .state = true;
                                  ++ref
                                      .read(currentDayIndexProvider.notifier)
                                      .state;
                                }
                              : null,
                          icon: const Icon(Icons.chevron_right),
                        ),
                        startFirstAni: startFirstAni,
                        makeShimmer: ref.watch(currentDayIndexProvider) != 4,
                        token: 6,
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: screenHeight / 10 * 6,
                  width: screenWidth,
                  child: AnimateThisWidget(
                    key: Key('temp$index'),
                    widget: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        '${fiveDaysWeatherList.forecastData[index * 8].temperature.currentTemperature}°C',
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          fontSize: 45,
                        ),
                      ),
                    ),
                    startSwapAni: startSwapAni,
                    startFirstAni: startFirstAni,
                    token: 3,
                  ),
                ),
                Positioned(
                  top: screenHeight / 10 * 7,
                  width: screenWidth,
                  child: AnimateThisWidget(
                    key: Key('condition$index'),
                    widget: Text(
                      fiveDaysWeatherList.forecastData[index * 8].details.first
                          .weatherLongDescription.capitalizeFirstLetters,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 25,
                      ),
                    ),
                    startSwapAni: startSwapAni,
                    startFirstAni: startFirstAni,
                    token: 3,
                  ),
                ),
                Positioned(
                  top: screenHeight / 10 * 8,
                  width: screenWidth,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      AnimateThisWidget(
                        key: Key('humi$index'),
                        widget: Text(
                          'Ỽ ${fiveDaysWeatherList.forecastData[index * 8].temperature.humidity}%',
                          style: const TextStyle(
                            fontSize: 25,
                          ),
                        ),
                        startSwapAni: startSwapAni,
                        startFirstAni: startFirstAni,
                        token: 4,
                      ),
                      AnimateThisWidget(
                        key: Key('wind$index'),
                        widget: Text(
                          '~~ ${fiveDaysWeatherList.forecastData[index * 8].wind.speed}Mph',
                          style: const TextStyle(
                            fontSize: 25,
                          ),
                        ),
                        startSwapAni: startSwapAni,
                        startFirstAni: startFirstAni,
                        token: 5,
                      ),
                    ],
                  ),
                ),
              ],
              MyFooter(
                startFirstAni: startFirstAni,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
