import 'package:device_preview/device_preview.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:open_weather_client/open_weather.dart';
import 'package:weather_app/app_consts.dart';

import 'screens/home_screen.dart';

// WeatherService? wService;
// GeocodingService? gService;

OpenWeather? openWeather;

void main() {
  Animate.restartOnHotReload = true;
  // wService = WeatherService(apiKey);
  // gService = GeocodingService(apiKey);
  openWeather = OpenWeather(apiKey: apiKey);
  runApp(
    DevicePreview(
      enabled: !kReleaseMode,
      builder: (context) => const ProviderScope(
        child: MyApp(),
      ),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Weather App',
      useInheritedMediaQuery: true,
      locale: DevicePreview.locale(context),
      builder: DevicePreview.appBuilder,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const HomeScreen(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    Widget myWidget = Container(
      color: Colors.black,
      padding: const EdgeInsets.all(16),
      child: const Text(
        'WARNING!',
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      ),
    )
        .animate(
          onPlay: ((controller) => controller.repeat(reverse: true)),
        )
        .scaleXY(end: 1.1, curve: Curves.easeInOutCubic)
        .tint(color: Colors.red, end: 0.6)
        .elevation(end: 20);
    return Scaffold(
      body: Center(
        child: myWidget,
      ),
    );
  }
}
