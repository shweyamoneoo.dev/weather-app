extension StringExtend on String {
  String get capitalizeFirst =>
      length > 0 ? '${this[0].toUpperCase()}${substring(1).toLowerCase()}' : '';

  String get capitalizeFirstLetters =>
      split(' ').map((String e) => e.capitalizeFirst).join(' ');
}
