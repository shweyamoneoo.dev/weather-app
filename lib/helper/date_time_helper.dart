import 'package:intl/intl.dart';

class DateTimeHelper {
  static String getDateTime(int time, int index) {
    String date = DateFormat.MMMEd()
        .format(DateTime.fromMillisecondsSinceEpoch(time * 1000));
    if (index < 2) {
      int commanIndex = date.indexOf(',');
      if (index == 0) {
        return 'Today, ${date.substring(commanIndex + 2)}';
      } else {
        return 'Tomorrow, ${date.substring(commanIndex + 2)}';
      }
    }
    return date;
  }
}
